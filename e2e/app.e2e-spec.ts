import { HtmlSelectRestfulAngularPage } from './app.po';

describe('html-select-restful-angular App', () => {
  let page: HtmlSelectRestfulAngularPage;

  beforeEach(() => {
    page = new HtmlSelectRestfulAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
