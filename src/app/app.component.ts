import { Component, OnInit } from '@angular/core';
import { Http } from "@angular/http";
import { FormGroup, FormBuilder, FormArray, FormControl } from "@angular/forms";
import { Subscription } from "rxjs/Subscription";
 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
   
  // property which will hold our form
  private formOption: FormGroup = new FormGroup({})
 
  // property which will hold the data for the <select> <option>
  private supportedSheduledTypes: FormArray = new FormArray([]);
  private schedulerTypeSubscription: Subscription;
 
  constructor(private http: Http, private fb: FormBuilder) {
  }
 
  ngOnInit() {
    // Fetch list of hero to be used in drop down and keep it as an instance veriable.
    this.fetchDefaultSupportedSchedledTypes();
 
    this.formOption = this.fb.group({
      scheduleTypes: this.supportedSheduledTypes
    });
  }
 
  /**
   * Fetch list of hero to be used in drop down and keep it as an instance veriable.
   */
  fetchDefaultSupportedSchedledTypes() {
    this.schedulerTypeSubscription = this.http.get('https://greenhorn-c7a65.firebaseio.com/hero.json').subscribe(
      (response) => {
        const data = response.json();
        this.supportedSheduledTypes = this.createFormArrayForScheduledTypes(data);
      },
      (error) => {
        console.log('error: ' + error);
      }
    );
  }
 
  /**
   * Create and FormArray of the given list of hero's
   * 
   * @param fetchedScheduleTypes 
   */
  createFormArrayForScheduledTypes(fetchedScheduleTypes: any): FormArray {
    let scheduledTypes = new FormArray([]);
    console.log('fetchedScheduleTypes length: ' + fetchedScheduleTypes.length);
    for (let entry in fetchedScheduleTypes) {
      console.log(fetchedScheduleTypes[entry]);
      scheduledTypes.push(new FormControl(fetchedScheduleTypes[entry]));
    }
    return scheduledTypes;
  }
 
  get scheduleTypes(): FormArray {
    return this.supportedSheduledTypes as FormArray;
  }
 
  onSelectType(htmlElement: any) {
    let element = htmlElement;
    console.log('element id: ' + element.value);
  }
 
  ngOnDestroy(): void {
    this.schedulerTypeSubscription.unsubscribe();
  }
}
